const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const DIST_DIR = path.join(__dirname, './public');
app.use(express.static(DIST_DIR));
const port = process.env.PORT || 8080;

app.get('/', (req, res) => {
  res.send('Working');
});

app.listen(port);
